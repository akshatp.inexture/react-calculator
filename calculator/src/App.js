import React, { Component } from 'react';
import './index.css';
import ResultComponent from './components/Result';
import KeyComponent from "./components/Keys";

class App extends Component {
    constructor(){
        super();

        this.state = {
            result: ""
        }

        this.keys = ['0' , '1' , '2' , '3' , '4' , '5' , '6' , '7' , '8' , '9'];
    }

    suffleKeys = () => {
      for (let i = this.keys.length - 1; i > 0; i--) {

          // Generate random number
          let j = Math.floor(Math.random() * (i + 1));
                      
          let temp = this.keys[i];
          this.keys[i] = this.keys[j];
          this.keys[j] = temp;

      }
        
    }

    onClick = button => {

        if(button === "="){
            this.calculate()
        }

        else if(button === "C"){
            this.reset()
        }
        else if(button === "Clear"){
            this.backspace()
        }

        else {
          this.suffleKeys();
          
            this.setState({
                result: this.state.result + button
            })
        }
    };


    calculate = () => {
        var checkResult = ''
        if(this.state.result.includes('--')){
            checkResult = this.state.result.replace('--','+')
        }

        else {
            checkResult = this.state.result
        }

        
            this.setState({
                // eslint-disable-next-line
                result: (eval(checkResult) || "" ) + ""
            })
        
    };

    reset = () => {
        this.setState({
            result: ""
        })
    };

    backspace = () => {
        this.setState({
            result: this.state.result.slice(0, -1)
        })
    };

    render() {
        return (
            <div>
                <div className="calculator-body">
                    <h1>Calculator</h1>
                    <ResultComponent result={this.state.result}/>
                    <KeyComponent onClick={this.onClick} keys={this.keys}/>
                </div>
            </div>
        );
    }
}

export default App;