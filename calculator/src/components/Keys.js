import React, {Component } from 'react';

class KeyPadComponent extends Component {

    render() {
        
        return (
            <div className="button">
                <button name="(" onClick={a => this.props.onClick(a.target.name)}>(</button>
                <button name="Clear" onClick={a => this.props.onClick(a.target.name)}>Clear</button>
                <button name=")" onClick={a => this.props.onClick(a.target.name)}>)</button>
                <button name="C" onClick={a => this.props.onClick(a.target.name)}>C</button><br/>


                <button name={this.props.keys[1]} onClick={a => this.props.onClick(a.target.name)}>{this.props.keys[1]}</button>
                <button name={this.props.keys[2]} onClick={a => this.props.onClick(a.target.name)}>{this.props.keys[2]}</button>
                <button name={this.props.keys[3]} onClick={a => this.props.onClick(a.target.name)}>{this.props.keys[3]}</button>
                <button className='operator' name="+" onClick={a => this.props.onClick(a.target.name)}>+</button><br/>


                <button name={this.props.keys[4]} onClick={a => this.props.onClick(a.target.name)}>{this.props.keys[4]}</button>
                <button name={this.props.keys[5]} onClick={a => this.props.onClick(a.target.name)}>{this.props.keys[5]}</button>
                <button name={this.props.keys[6]} onClick={a => this.props.onClick(a.target.name)}>{this.props.keys[6]}</button>
                <button className='operator' name="-" onClick={a => this.props.onClick(a.target.name)}>-</button><br/>

                <button name={this.props.keys[7]} onClick={a => this.props.onClick(a.target.name)}>{this.props.keys[7]}</button>
                <button name={this.props.keys[8]} onClick={a => this.props.onClick(a.target.name)}>{this.props.keys[8]}</button>
                <button name={this.props.keys[9]} onClick={a => this.props.onClick(a.target.name)}>{this.props.keys[9]}</button>
                <button className='operator' name="*" onClick={a => this.props.onClick(a.target.name)}>x</button><br/>


                <button name="." onClick={a => this.props.onClick(a.target.name)}>.</button>
                <button name={this.props.keys[0]} onClick={a => this.props.onClick(a.target.name)}>{this.props.keys[0]}</button>
                <button name="=" onClick={a => this.props.onClick(a.target.name)}>=</button>
                <button className='operator' name="/" onClick={a => this.props.onClick(a.target.name)}>÷</button><br/>
            </div>
        );
    }
}


export default KeyPadComponent;
